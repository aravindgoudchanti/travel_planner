<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth.models import AbstractUser


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cab_driver = models.ForeignKey('CabDriver', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.user.username

class Feedback(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def _str_(self):
        return self.name    

class RoomNumber(models.Model):
    room_number = models.IntegerField()
    available = models.BooleanField(default=True)

<<<<<<< HEAD
class HotelBooking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
=======
<<<<<<< HEAD
class HotelBooking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
=======
from django.conf import settings


class Booking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
#     full_name = models.CharField(max_length=100)
#     email = models.EmailField()
#     mobile = models.CharField(max_length=10)
#     package = models.CharField(max_length=100)
#     num_persons = models.PositiveIntegerField()
#     boarding = models.BooleanField(default=False)
#     fooding = models.BooleanField(default=False)
#     sight_seeing = models.BooleanField(default=False)
#     coupon_code = models.CharField(max_length=50, null=True, blank=True)
#     terms = models.BooleanField()
    
#     def __str__(self):
#         return self.user.username
    
    
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    date_of_birth = models.DateField(blank=True, null=True)
    mobile_number = models.CharField(max_length=20, blank=True, null=True)

    class Meta(AbstractUser.Meta):
        pass

User._meta.get_field('groups').remote_field.related_name = 'user_custom_set'
User._meta.get_field('user_permissions').remote_field.related_name = 'user_custom_set'


from django.db import models

class HotelBooking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

>>>>>>> 34fcee235fe2362a41de843fc81200791d697d58
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
    name = models.CharField(max_length=100)
    email = models.EmailField()
    check_in_date = models.DateField()
    check_out_date = models.DateField()
    num_guests = models.IntegerField()
    room_number = models.ForeignKey(RoomNumber, on_delete=models.SET_NULL, null=True, blank=True)
    canceled = models.BooleanField(default=False)
    cancellation_reason = models.TextField(blank=True)

    def __str__(self):
        return self.user.username

from django.core.validators import MaxValueValidator
import datetime
from itertools import islice


class BusDriver(models.Model):
    name = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=10)
    bus_number = models.CharField(max_length=20)
    available = models.BooleanField(default=True)

<<<<<<< HEAD

    def __str__(self):
        return self.name
class BusBooking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=100)
    email = models.EmailField()
    mobile = models.CharField(max_length=10)
    source = models.CharField(max_length=100)
    destination = models.CharField(max_length=100)
    travel_date = models.DateField()
    num_passengers = models.PositiveIntegerField(validators=[MaxValueValidator(10)])
    boarding = models.BooleanField(default=False)
    fooding = models.BooleanField(default=False)
    sight_seeing = models.BooleanField(default=False)
    # terms = models.BooleanField()
    seat_numbers = models.CharField(max_length=100, null=True, blank=True)
    bus_driver = models.ForeignKey(BusDriver, on_delete=models.SET_NULL, null=True, blank=True)
    canceled = models.BooleanField(default=False)
    cancellation_reason = models.TextField(blank=True)

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        if not self.seat_numbers:
            seat_numbers = self.assign_seat_numbers(self.num_passengers)
            self.seat_numbers = ', '.join(seat_numbers)
        super().save(*args, **kwargs)

    @staticmethod
    def assign_seat_numbers(num_passengers):
        total_seats = 50
        booked_seats = BusBooking.objects.filter(travel_date=datetime.date.today()).values_list(
            'seat_numbers', flat=True
        )
        available_seats = set(range(1, total_seats + 1)).difference(
            int(seat) for seats in booked_seats for seat in seats.split(', ')
        )
        assigned_seats = list(islice(available_seats, num_passengers))
        return [str(seat) for seat in sorted(assigned_seats)]


class CabDriver(models.Model):
    name = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=20)
    cab_number = models.CharField(max_length=20)
    available = models.BooleanField(default=True)
=======
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428

    def __str__(self):
        return self.name
class BusBooking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=100)
    email = models.EmailField()
    mobile = models.CharField(max_length=10)
    source = models.CharField(max_length=100)
    destination = models.CharField(max_length=100)
    travel_date = models.DateField()
    num_passengers = models.PositiveIntegerField(validators=[MaxValueValidator(10)])
    boarding = models.BooleanField(default=False)
    fooding = models.BooleanField(default=False)
    sight_seeing = models.BooleanField(default=False)
    # terms = models.BooleanField()
    seat_numbers = models.CharField(max_length=100, null=True, blank=True)
    bus_driver = models.ForeignKey(BusDriver, on_delete=models.SET_NULL, null=True, blank=True)
    canceled = models.BooleanField(default=False)
    cancellation_reason = models.TextField(blank=True)

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        if not self.seat_numbers:
            seat_numbers = self.assign_seat_numbers(self.num_passengers)
            self.seat_numbers = ', '.join(seat_numbers)
        super().save(*args, **kwargs)

    @staticmethod
    def assign_seat_numbers(num_passengers):
        total_seats = 50
        booked_seats = BusBooking.objects.filter(travel_date=datetime.date.today()).values_list(
            'seat_numbers', flat=True
        )
        available_seats = set(range(1, total_seats + 1)).difference(
            int(seat) for seats in booked_seats for seat in seats.split(', ')
        )
        assigned_seats = list(islice(available_seats, num_passengers))
        return [str(seat) for seat in sorted(assigned_seats)]


<<<<<<< HEAD
from django.db import models
from django.conf import settings

class CabBooking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=100)
    email = models.EmailField()
    mobile = models.CharField(max_length=10)
    pickup_location = models.CharField(max_length=100)
    drop_location = models.CharField(max_length=100)
    travel_date = models.DateField()
    num_passengers = models.PositiveIntegerField(validators=[MaxValueValidator(4)])
    boarding = models.BooleanField(default=False)
    fooding = models.BooleanField(default=False)
    sight_seeing = models.BooleanField(default=False)
    cab_driver = models.ForeignKey(CabDriver, on_delete=models.SET_NULL, null=True, blank=True)
    # terms = models.BooleanField()
    canceled = models.BooleanField(default=False)
    cancellation_reason = models.TextField(blank=True)

    def __str__(self):
        return self.user.username

=======
class CabDriver(models.Model):
    name = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=20)
    cab_number = models.CharField(max_length=20)
    available = models.BooleanField(default=True)

    def __str__(self):
<<<<<<< HEAD
        return self.name

=======
        return self.user.username
>>>>>>> 34fcee235fe2362a41de843fc81200791d697d58

from django.db import models
from django.conf import settings

<<<<<<< HEAD
=======
class BusBooking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=100)
    email = models.EmailField()
    mobile = models.CharField(max_length=10)
    source = models.CharField(max_length=100)
    destination = models.CharField(max_length=100)
    travel_date = models.DateField()
    num_passengers = models.PositiveIntegerField()
    boarding = models.BooleanField(default=False)
    fooding = models.BooleanField(default=False)
    sight_seeing = models.BooleanField(default=False)
    # coupon_code = models.CharField(max_length=50, null=True, blank=True)
    terms = models.BooleanField()

    def __str__(self):
        return self.user.username

>>>>>>> 34fcee235fe2362a41de843fc81200791d697d58
class CabBooking(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=100)
    email = models.EmailField()
    mobile = models.CharField(max_length=10)
    pickup_location = models.CharField(max_length=100)
    drop_location = models.CharField(max_length=100)
    travel_date = models.DateField()
<<<<<<< HEAD
    num_passengers = models.PositiveIntegerField(validators=[MaxValueValidator(4)])
    boarding = models.BooleanField(default=False)
    fooding = models.BooleanField(default=False)
    sight_seeing = models.BooleanField(default=False)
    cab_driver = models.ForeignKey(CabDriver, on_delete=models.SET_NULL, null=True, blank=True)
    # terms = models.BooleanField()
    canceled = models.BooleanField(default=False)
    cancellation_reason = models.TextField(blank=True)

    def __str__(self):
        return self.user.username
=======
    num_passengers = models.PositiveIntegerField()
    boarding = models.BooleanField(default=False)
    fooding = models.BooleanField(default=False)
    sight_seeing = models.BooleanField(default=False)
    # coupon_code = models.CharField(max_length=50, null=True, blank=True)
    terms = models.BooleanField()
>>>>>>> 34fcee235fe2362a41de843fc81200791d697d58

    def __str__(self):
        return self.user.username
>>>>>>> a3fc64f407a8e3dedfbeaa2262048bc1e5b85088
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
