<<<<<<< HEAD
=======
from django import forms
from django.contrib.auth.forms import UserCreationForm, User
from .models import BusBooking, CabBooking, HotelBooking, Feedback
from bootstrap_datepicker_plus.widgets import DatePickerInput
from django.utils import timezone
from datetime import timedelta

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['name', 'email', 'message']


class ContactForm(forms.Form):
    name = forms.CharField(max_length=100)
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)


class SignUpForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['name', 'email', 'message']


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput)


<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
class BusBookingForm(forms.ModelForm):
    travel_date = forms.DateField(
        widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'})
    )
<<<<<<< HEAD

    class Meta:
        model = BusBooking
        fields = [
            'full_name',
            'email',
            'mobile',
            'source',
            'destination',
            'travel_date',
            'num_passengers',
            'boarding',
            'fooding',
            'sight_seeing',
            # 'terms',
        ]
        widgets = {
            'boarding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'fooding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'sight_seeing': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        #     'terms': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }

    def clean(self):
        cleaned_data = super().clean()
        travel_date = cleaned_data.get('travel_date')
        num_passengers = cleaned_data.get('num_passengers')

        if travel_date:
            today = timezone.now().date()
            tomorrow = today + timedelta(days=1)

            if travel_date < today:
                self.add_error('travel_date', 'Travel date must be today or later.')

            if travel_date > tomorrow:
                self.add_error('travel_date', 'Travel date must be today or tomorrow.')

        if num_passengers and num_passengers > 50:
            self.add_error('num_passengers', 'Number of passengers cannot exceed 50.')

        return cleaned_data




class CabBookingForm(forms.ModelForm):
    travel_date = forms.DateField(
        widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'})
    )

    class Meta:
        model = CabBooking
        fields = [
            'full_name',
            'email',
            'mobile',
            'pickup_location',
            'drop_location',
            'travel_date',
            'num_passengers',
            'boarding',
            'fooding',
            'sight_seeing',
            # 'terms',
        ]
        widgets = {
            'boarding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'fooding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'sight_seeing': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            # 'terms': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }

    def clean(self):
        cleaned_data = super().clean()
        travel_date = cleaned_data.get('travel_date')
        num_passengers = cleaned_data.get('num_passengers')

        if travel_date:
            today = timezone.now().date()
            tomorrow = today + timedelta(days=1)

            if travel_date < today:
                self.add_error('travel_date', 'Travel date must be today or later.')

            if travel_date > tomorrow:
                self.add_error('travel_date', 'Travel date must be today or tomorrow.')

        if num_passengers and num_passengers > 4:
            self.add_error('num_passengers', 'Number of passengers cannot exceed 4.')

        return cleaned_data

=======

    class Meta:
        model = BusBooking
        fields = [
            'full_name',
            'email',
            'mobile',
            'source',
            'destination',
            'travel_date',
            'num_passengers',
            'boarding',
            'fooding',
            'sight_seeing',
            # 'terms',
        ]
        widgets = {
            'boarding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'fooding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'sight_seeing': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        #     'terms': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }

    def clean(self):
        cleaned_data = super().clean()
        travel_date = cleaned_data.get('travel_date')
        num_passengers = cleaned_data.get('num_passengers')

        if travel_date:
            today = timezone.now().date()
            tomorrow = today + timedelta(days=1)

            if travel_date < today:
                self.add_error('travel_date', 'Travel date must be today or later.')

            if travel_date > tomorrow:
                self.add_error('travel_date', 'Travel date must be today or tomorrow.')

        if num_passengers and num_passengers > 50:
            self.add_error('num_passengers', 'Number of passengers cannot exceed 50.')

        return cleaned_data




class CabBookingForm(forms.ModelForm):
    travel_date = forms.DateField(
        widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'})
    )

    class Meta:
        model = CabBooking
        fields = [
            'full_name',
            'email',
            'mobile',
            'pickup_location',
            'drop_location',
            'travel_date',
            'num_passengers',
            'boarding',
            'fooding',
            'sight_seeing',
            # 'terms',
        ]
        widgets = {
            'boarding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'fooding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'sight_seeing': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            # 'terms': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }

    def clean(self):
        cleaned_data = super().clean()
        travel_date = cleaned_data.get('travel_date')
        num_passengers = cleaned_data.get('num_passengers')

        if travel_date:
            today = timezone.now().date()
            tomorrow = today + timedelta(days=1)

            if travel_date < today:
                self.add_error('travel_date', 'Travel date must be today or later.')

            if travel_date > tomorrow:
                self.add_error('travel_date', 'Travel date must be today or tomorrow.')

        if num_passengers and num_passengers > 4:
            self.add_error('num_passengers', 'Number of passengers cannot exceed 4.')

        return cleaned_data

=======
from django import forms
from .models import HotelBooking

class HotelBookingForm(forms.ModelForm):
    class Meta:
        model = HotelBooking
        fields = [
            'name',
            'email',
            'check_in_date',
            'check_out_date',
            'num_guests',
        ]


from django import forms
from .models import BusBooking, CabBooking

class BusBookingForm(forms.ModelForm):
    class Meta:
        model = BusBooking
        fields = [
            'full_name',
            'email',
            'mobile',
            'source',
            'destination',
            'travel_date',
            'num_passengers',
            'boarding',
            'fooding',
            'sight_seeing',
            # 'coupon_code',
            'terms',
        ]
        widgets = {
            'travel_date': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'boarding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'fooding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'sight_seeing': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'terms': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }

class CabBookingForm(forms.ModelForm):
    class Meta:
        model = CabBooking
        fields = [
            'full_name',
            'email',
            'mobile',
            'pickup_location',
            'drop_location',
            'travel_date',
            'num_passengers',
            'boarding',
            'fooding',
            'sight_seeing',
            # 'coupon_code',
            'terms',
        ]
        widgets = {
            'travel_date': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'boarding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'fooding': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'sight_seeing': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'terms': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        }












from django import forms
from .models import HotelBooking

from bootstrap_datepicker_plus.widgets import DatePickerInput


class BookingForm(forms.ModelForm):
    check_in_date = forms.DateField(widget=DatePickerInput())
    check_out_date = forms.DateField(widget=DatePickerInput())

    class Meta:
        model = HotelBooking
        fields = ['name', 'email', 'check_in_date', 'check_out_date', 'num_guests']
>>>>>>> a3fc64f407a8e3dedfbeaa2262048bc1e5b85088
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428


class YourForm(forms.Form):
   your_date_field = forms.DateField(widget=DatePickerInput(options={'format': 'yyyy-mm-dd'}))

<<<<<<< HEAD
=======
<<<<<<< HEAD
=======

from django import forms
HotelBooking

>>>>>>> a3fc64f407a8e3dedfbeaa2262048bc1e5b85088
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
class HotelBookingForm(forms.ModelForm):
    check_in_date = forms.DateField(
        widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'})
    )
    check_out_date = forms.DateField(
        widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'})
    )

    class Meta:
        model = HotelBooking
        fields = ['name', 'email', 'check_in_date', 'check_out_date', 'num_guests']
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428

    def clean(self):
        cleaned_data = super().clean()
        check_in_date = cleaned_data.get('check_in_date')
        check_out_date = cleaned_data.get('check_out_date')
        num_guests = cleaned_data.get('num_guests')

        if check_in_date and check_out_date:
            today = timezone.now().date()
            tomorrow = today + timedelta(days=1)
            min_check_out_date = check_in_date + timedelta(days=3)

            if check_in_date < today:
                self.add_error('check_in_date', 'Check-in date must be today or later.')

            if check_in_date > tomorrow:
                self.add_error('check_in_date', 'Check-in date must be today or tomorrow.')

            if check_out_date > min_check_out_date:
                self.add_error('check_out_date', 'Check-out date must be at least three days after the check-in date.')

        if num_guests and num_guests > 4:
            self.add_error('num_guests', 'Number of guests cannot exceed 4 per room.')

        return cleaned_data

        
class CancellationForm(forms.Form):
<<<<<<< HEAD
    reason = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}))
=======
    reason = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}))
=======
>>>>>>> 34fcee235fe2362a41de843fc81200791d697d58
>>>>>>> a3fc64f407a8e3dedfbeaa2262048bc1e5b85088
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
