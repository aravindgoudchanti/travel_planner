<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======


from django.shortcuts import render, redirect
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
<<<<<<< HEAD
from .forms import FeedbackForm,ContactForm, SignUpForm, BusBookingForm, CabBookingForm, HotelBookingForm, CancellationForm
from .models import BusBooking, CabBooking,CabDriver, BusDriver, RoomNumber, HotelBooking
from django.shortcuts import render, redirect, get_object_or_404
=======
from .forms import TripForm,FeedbackForm,ContactForm
from .models import Booking
from .forms import SignUpForm
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect('home')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

def logout_view(request):
    logout(request)
    return redirect('home')


def home(request):
    return render(request, 'home.html')

<<<<<<< HEAD
def booking_form(request):
=======
from django.contrib.auth.decorators import login_required

@login_required
def book_bus(request):
    if request.method == 'POST':
        full_name = request.POST.get('full_name')
        email = request.POST.get('email')
        mobile = request.POST.get('mobile')
        package = request.POST.get('package')
        num_persons = request.POST.get('num_persons')
        boarding = 'boarding' in request.POST
        fooding = 'fooding' in request.POST
        sight_seeing = 'sight_seeing' in request.POST
        coupon_code = request.POST.get('coupon_code')
        terms = request.POST.get('terms')

        booking = Booking(
            user=request.user,
            full_name=full_name,
            email=email,
            mobile=mobile,
            package=package,
            num_persons=num_persons,
            boarding=boarding,
            fooding=fooding,
            sight_seeing=sight_seeing,
            coupon_code=coupon_code,
            terms=terms == 'true'
        )

        booking.save()

        return redirect('success')

    return render(request, 'busses.html')


from django.shortcuts import render

def success(request):
    return render(request, 'success.html')


def cabs(request):
    if request.method == 'POST':
        full_name = request.POST.get('full_name')
        email = request.POST.get('email')
        mobile = request.POST.get('mobile')
        package = request.POST.get('package')
        num_persons = request.POST.get('num_persons')
        boarding = 'boarding' in request.POST
        fooding = 'fooding' in request.POST
        sight_seeing = 'sight_seeing' in request.POST
        coupon_code = request.POST.get('coupon_code')
        terms = request.POST.get('terms')

        booking = Booking(
            user=request.user,
            full_name=full_name,
            email=email,
            mobile=mobile,
            package=package,
            num_persons=num_persons,
            boarding=boarding,
            fooding=fooding,
            sight_seeing=sight_seeing,
            coupon_code=coupon_code,
            terms=terms == 'true'
        )

        booking.save()

        return redirect('success')

    return render(request, 'cabs.html')


from django.contrib.auth.decorators import login_required

@login_required
def book_hotel(request):
    if request.method == 'POST':
        form = HotelBookingForm(request.POST)
        if form.is_valid():
            booking = form.save(commit=False)
            booking.user = request.user
            booking.save()
            return render(request, 'booking_success.html')
    else:
        form = HotelBookingForm()
    
    return render(request, 'booking.html', {'form': form})

def reservation_form(request):
    if request.method == 'POST':
        # Process the form submission here
        pass
    return render(request, 'cabs.html')


def booking_form(request):
    # if request.method == 'POST':
    #     # Process the form submission
    #     first_name = request.POST.get('first_name')
    #     last_name = request.POST.get('last_name')
    #     phone_number = request.POST.get('phone_number')
    #     email = request.POST.get('email')
    #     departure_date = request.POST.get('departure_date')
    #     arrival_date = request.POST.get('arrival_date')

    #     booking = Booking(
    #         first_name=first_name,
    #         last_name=last_name,
    #         phone_number=phone_number,
    #         email=email,
    #         departure_date=departure_date,
    #         arrival_date=arrival_date,
    #     )
    #     booking.save()  


>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
    return render(request, 'booking_form.html')

def about(request):
    return render(request,'about.html')

def packages(request):
    return render(request,'packages.html')

def contact(request):
    return render(request,'contact.html')

def attractions(request):
    return render(request,'attractions.html')

@login_required
def feedback(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('thank_you')
    else:
        form = FeedbackForm()
    
    return render(request, 'feedback.html', {'form': form})

def thank_you(request):
    return render(request, 'thank_you.html')

def contact_us(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
                        
            return render(request, 'contact_success.html')
    else:
        form = ContactForm()
    
    return render(request, 'contact_us.html', {'form': form})
<<<<<<< HEAD
    
def profile(request):
    if request.user.is_authenticated:
        hotel_bookings = HotelBooking.objects.filter(user=request.user, canceled=False)
        bus_bookings = BusBooking.objects.filter(user=request.user, canceled=False)
        cab_bookings = CabBooking.objects.filter(user=request.user, canceled=False)

        context = {
            'user': request.user,
            'hotel_bookings': hotel_bookings,
            'bus_bookings': bus_bookings,
            'cab_bookings': cab_bookings
        }
        return render(request, 'profile.html', context)
    else:
        return redirect('login')

def cancel_booking(request, plan_id): 
    return redirect('booking_canceled')

def booking_success(request):
    return render(request, 'booking_success.html')

from .forms import HotelBookingForm

def booking_cancelled(request):
    return render(request, 'booking_cancelled.html')

def bus_booking(request):
    if request.method == 'POST':
        form = BusBookingForm(request.POST)
        if form.is_valid():
            bus_booking = form.save(commit=False)
            bus_booking.user = request.user

            bus_driver = BusDriver.objects.filter(available=True).first()
            if bus_driver:
                bus_booking.bus_driver = bus_driver
                bus_driver.available = False
                bus_driver.save()

            bus_booking.save()
            return redirect('booking_success')
    else:
        form = BusBookingForm()

    return render(request, 'bus_booking.html', {'form': form, 'bus_bookings': BusBooking.objects.all()})


def cab_booking(request):
    if request.method == 'POST':
        form = CabBookingForm(request.POST)
        if form.is_valid():
            cab_booking = form.save(commit=False)
            cab_booking.user = request.user

            cab_driver = CabDriver.objects.filter(available=True).first()
            if cab_driver:
                cab_booking.cab_driver = cab_driver
                cab_driver.available = False
                cab_driver.save()

            cab_booking.save()
            return redirect('booking_success')
    else:
        form = CabBookingForm()

    return render(request, 'cab_booking.html', {'form': form, 'cab_bookings': CabBooking.objects.all()})


def cancel_bus_booking(request, booking_id):
    booking = BusBooking.objects.get(id=booking_id)

    if request.method == 'POST':
        form = CancellationForm(request.POST)
        if form.is_valid():
            booking.canceled = True
            booking.cancellation_reason = form.cleaned_data['reason']
            booking.save()
            return redirect('profile')
    else:
        form = CancellationForm()

    return render(request, 'cancel_booking.html', {'form': form, 'booking': booking})


def cancel_cab_booking(request, booking_id):
    booking = CabBooking.objects.get(id=booking_id)
    if request.method == 'POST':
        form = CancellationForm(request.POST)
        if form.is_valid():
            booking.canceled = True
            booking.cancellation_reason = form.cleaned_data['reason']
            booking.save()
            return redirect('profile')
    else:
        form = CancellationForm()

    return render(request, 'cancel_booking.html', {'form': form, 'booking': booking})



def hotel_booking(request):
    if request.method == 'POST':
        form = HotelBookingForm(request.POST)
        if form.is_valid():
            hotel_booking = form.save(commit=False)
            hotel_booking.user = request.user

            room_number = RoomNumber.objects.filter(available=True).first()
            if room_number:
                hotel_booking.room_number = room_number
                room_number.available = False
                room_number.save()

            hotel_booking.save()
            return redirect('booking_success')
    else:
        form = HotelBookingForm()

    return render(request, 'hotel_booking.html', {'form': form, 'hotel_bookings': HotelBooking.objects.all()})
=======

from django.shortcuts import render
from .models import Booking, HotelBooking

@login_required(login_url='/login/')

# def profile(request):
#     if request.user.is_authenticated:
#         bookings = Booking.objects.filter(user=request.user)
#         hotel_bookings = hotelbooking.objects.filter(user=request.user)

#         context = {
#             'user': request.user,
#             'bookings': bookings,
#             'hotel_bookings': hotel_bookings
#         }
#         return render(request, 'profile.html', context)
#     else:
#         return redirect('login')
    
def profile(request):
    if request.user.is_authenticated:
        hotel_bookings = HotelBooking.objects.filter(user=request.user)
        bus_bookings = BusBooking.objects.filter(user=request.user)
        cab_bookings = CabBooking.objects.filter(user=request.user)

        context = {
            'user': request.user,
            'hotel_bookings': hotel_bookings,
            'bus_bookings': bus_bookings,
            'cab_bookings': cab_bookings
        }
        return render(request, 'profile.html', context)
    else:
        return redirect('login')

def planner(request):
    if request.method == 'POST':
        start_date = request.POST.get('start_date')
        end_date = request.POST.get('end_date')
        return redirect('plan')        
    return render(request, 'planning.html')

def plan(request):
    return render(request, 'plan.html')

from django.shortcuts import render
from .models import Activity, Profile


def allocated_plan_view(request):
    profile = Profile.objects.get(user=request.user)
    allocated_plan = Activity.objects.filter(profile=profile).first()

    context = {
        'allocated_plan': allocated_plan
    }
    return render(request, 'allocated_plan.html', context)

def allocated_plan(request):
    context = {
        'allocated_plan': allocated_plan
    }
    return render(request, 'allocated_plan.html', context)


def cancel_booking(request, plan_id): 
    return redirect('booking_canceled')
    
def booking_canceled(request):
    return render(request, 'booking_canceled.html')

>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428

def booking_success(request):
    return render(request, 'booking_success.html')


def cancel_hotel_booking(request, booking_id):
    booking = get_object_or_404(HotelBooking, id=booking_id, user=request.user)

<<<<<<< HEAD
=======


from .forms import HotelBookingForm

def hotel_booking(request):
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
    if request.method == 'POST':
        form = CancellationForm(request.POST)
        if form.is_valid():
<<<<<<< HEAD
            booking.canceled = True
            booking.cancellation_reason = form.cleaned_data['reason']
            booking.save()
            return redirect('profile')
    else:
        form = CancellationForm()

    return render(request, 'cancel_booking.html', {'form': form, 'booking': booking})

def cancelled_bookings(request):
    canceled_hotel_bookings = HotelBooking.objects.filter(user=request.user, canceled=True)
    canceled_bus_bookings = BusBooking.objects.filter(user=request.user, canceled=True)
    canceled_cab_bookings = CabBooking.objects.filter(user=request.user, canceled=True)


    return render(request, 'cancelled_bookings.html', {'canceled_hotel_bookings': canceled_hotel_bookings, 'canceled_bus_bookings': canceled_bus_bookings, 'canceled_cab_bookings': canceled_cab_bookings})


=======
            hotel_booking = form.save(commit=False)
            hotel_booking.user = request.user
            hotel_booking.save()
            return redirect('booking_success')
    else:
        form = HotelBookingForm()

    return render(request, 'booking.html', {'form': form})

from django.shortcuts import redirect, render
from .models import HotelBooking

def cancel_hotelbooking(request, booking_id):
    booking = HotelBooking.objects.get(id=booking_id)

    if booking.user == request.user:
        booking.delete()
        return redirect('booking_cancelled')

    return redirect('profile')

def booking_cancelled(request):
    return render(request, 'booking_cancelled.html')

from django.shortcuts import render, redirect
from .forms import BusBookingForm, CabBookingForm
from .models import BusBooking, CabBooking

def bus_booking(request):
    if request.method == 'POST':
        form = BusBookingForm(request.POST)
        if form.is_valid():
            bus_booking = form.save(commit=False)
            bus_booking.user = request.user
            bus_booking.save()
            return redirect('booking_success')
    else:
        form = BusBookingForm()

    return render(request, 'bus_booking.html', {'form': form})

def cancel_bus_booking(request, booking_id):
    booking = BusBooking.objects.get(id=booking_id)

    if booking.user == request.user:
        booking.delete()
        return redirect('booking_cancelled')

    return redirect('profile')

def cab_booking(request):
    if request.method == 'POST':
        form = CabBookingForm(request.POST)
        if form.is_valid():
            cab_booking = form.save(commit=False)
            cab_booking.user = request.user
            cab_booking.save()
            return redirect('booking_success')
    else:
        form = CabBookingForm()

    return render(request, 'cab_booking.html', {'form': form})

from django.shortcuts import render, redirect, get_object_or_404



def cancel_cab_booking(request, booking_id):
    booking = CabBooking.objects.get(id=booking_id)
    if booking.user == request.user:
        booking.delete()
        return redirect('booking_cancelled')

    return redirect('profile')
>>>>>>> 34fcee235fe2362a41de843fc81200791d697d58
>>>>>>> a3fc64f407a8e3dedfbeaa2262048bc1e5b85088
>>>>>>> 651ffcdfe0fb8c19f5fa8639bf7c95162cad9428
