# Generated by Django 4.2 on 2023-06-15 14:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planner', '0006_profile_activity_profile'),
    ]

    operations = [
        migrations.RenameField(
            model_name='booking',
            old_name='first_name',
            new_name='full_name',
        ),
        migrations.RenameField(
            model_name='booking',
            old_name='guest_count',
            new_name='num_persons',
        ),
        migrations.RenameField(
            model_name='booking',
            old_name='last_name',
            new_name='package',
        ),
        migrations.RemoveField(
            model_name='booking',
            name='arrival_date',
        ),
        migrations.RemoveField(
            model_name='booking',
            name='departure_date',
        ),
        migrations.RemoveField(
            model_name='booking',
            name='phone_number',
        ),
        migrations.RemoveField(
            model_name='booking',
            name='room_type',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='allocated_activity',
        ),
        migrations.AddField(
            model_name='booking',
            name='boarding',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='booking',
            name='coupon_code',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='fooding',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='booking',
            name='mobile',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='booking',
            name='sight_seeing',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='booking',
            name='terms',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='User',
        ),
    ]
