from django.contrib import admin
from .models import Trip,Activity, Profile
from .forms import TripForm

class TripAdmin(admin.ModelAdmin):
    form = TripForm

admin.site.register(Trip, TripAdmin)
admin.site.register(Activity)
admin.site.register(Profile)

