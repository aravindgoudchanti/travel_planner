from django.urls import path
from django.contrib import admin
from . import views
from planner import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('accounts/signup/', views.signup, name='signup'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('activities/', views.activity_list, name='activity_list'),
    path('activity/<int:activity_id>/', views.activity_detail, name='activity_detail'),
    path('activities/add/', views.add_activity, name='add_activity'),
    path('home', views.home, name='home'),
    path('busses/', views.busses, name='busses'),
    path('cabs/', views.cabs, name='cabs'),
    path('book-hotel/', views.book_hotel, name='book_hotel'),
    path('reservation-form/', views.reservation_form, name='reservation_form'),
    path('booking-form/', views.booking_form, name='booking_form'),
    path('about', views.about, name='about'),
    path('packages',views.packages,name='packages'),
    path('contact',views.contact,name='contact'),
    path('attractions',views.attractions,name='attractions'),
    path('feedback/', views.feedback, name='feedback'),
    path('thank-you/', views.thank_you, name='thank_you'),
    path('contactus/', views.contact_us, name='contactus'),
    path('profile/', views.profile, name='profile'),
    path('planner/', views.planner, name='planner'),
    path('plan/', views.plan, name='plan'),
    path('allocated-plan/', views.allocated_plan_view, name='allocated_plan'),


]