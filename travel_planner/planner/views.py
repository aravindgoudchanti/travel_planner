from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout as auth_logout, authenticate
from .models import Trip,Activity
from django.contrib.auth.decorators import login_required
from .forms import TripForm,FeedbackForm,ContactForm
from .models import Booking
from .forms import SignUpForm

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('planner')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        error_message = "Invalid username or password."

        return render(request, 'login.html', {'error_message': error_message})
    else:
        return render(request, 'login.html')

def logout(request):
    auth_logout(request)
    return redirect('login')

def activity_list(request):
    activities = Activity.objects.all()
    return render(request, 'planner/activity_list.html', {'activities': activities})

def activity_detail(request, activity_id):
    activity = Activity.objects.get(pk=activity_id)
    return render(request, 'planner/activity_detail.html', {'activity': activity})

def add_activity(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        location = request.POST.get('location')
        description = request.POST.get('description')

        activity = Activity(name=name, location=location, description=description)
        activity.save()

        return redirect('activity_list')

    return render(request, 'planner/add_activity.html')

def home(request):
    return render(request, 'home.html')


def busses(request):
    if request.method == 'POST':
       
        pass
    return render(request, 'busses.html')

def cabs(request):
    if request.method == 'POST':
       
        pass
    return render(request, 'cabs.html')


def book_hotel(request):
    if request.method == 'POST':
        
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        phone_number = request.POST.get('phone_number')
        email = request.POST.get('email')
        departure_date = request.POST.get('departure_date')
        arrival_date = request.POST.get('arrival_date')
        guest_count = request.POST.get('guest_count')
        room_type = request.POST.get('room_type')

        booking = Booking(
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number,
            email=email,
            departure_date=departure_date,
            arrival_date=arrival_date,
            guest_count=guest_count,
            room_type=room_type
        )
        booking.save()

        return render(request, 'booking_success.html')

    return render(request, 'book_hotel.html')


def reservation_form(request):
    if request.method == 'POST':
      
        pass
    return render(request, 'cabs.html')


def booking_form(request):
    if request.method == 'POST':
       
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        phone_number = request.POST.get('phone_number')
        email = request.POST.get('email')
        departure_date = request.POST.get('departure_date')
        arrival_date = request.POST.get('arrival_date')
    
        booking = Booking(
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number,
            email=email,
            departure_date=departure_date,
            arrival_date=arrival_date,
        )
        booking.save()  

    return render(request, 'booking_form.html')

def about(request):
    return render(request,'about.html')

def packages(request):
    return render(request,'packages.html')

def contact(request):
    return render(request,'contact.html')

def attractions(request):
    return render(request,'attractions.html')

def feedback(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('thank_you')
    else:
        form = FeedbackForm()
    
    return render(request, 'feedback.html', {'form': form})

def thank_you(request):
    return render(request, 'thank_you.html')

def contact_us(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
          
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            return redirect('contact_success')
            
           
    else:
        form = ContactForm()
    
    return render(request, 'contact_us.html', {'form': form})

def profile(request):

    return render(request, 'profile.html')


def planner(request):
    if request.method == 'POST':
        start_date = request.POST.get('start_date')
        end_date = request.POST.get('end_date')
        return redirect('plan')        
    return render(request, 'planning.html')

def plan(request):
    return render(request, 'plan.html')

from django.shortcuts import render
from .models import Activity

def allocated_plan_view(request):
    allocated_plan = Activity.objects.filter(profile__user=request.user).first()

    context = {
        'allocated_plan': allocated_plan
    }
    return render(request, 'allocated_plan.html', context)
